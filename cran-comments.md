## Test environments
* local (debian; R 3.4.4)
* rhub (debian-gcc-devel)
* rhub (windows-x86_64-devel)
* rhub (macos-elcapitan-release)

## R CMD check results
There were no ERRORs or WARNINGs. 

There was 1 NOTE:

* checking data for non-ASCII characters
     Note: found 7332 marked UTF-8 strings

We use a Serbian example data set with a lot of non-ASCII UTF-8 encoded
characters. For a package that deals with linguistic data this makes a lot of
sense to us. I searched for half an hour but could not find any way to mark it
non-ASCII on purpose to get rid of the note. Did I missed something? How to
handle it?


## Logs

### debian-gcc-devel

─  Building package
─  Uploading package
─  Preparing build, see status at
   http://builder.r-hub.io/status/ndl_0.2.18.tar.gz-bb2d728ca4664ad6a9bc452cc8632a29
─  Build started
─  Downloading and unpacking package file
─  Querying system requirements
─  Installing system requirements
─  Starting Docker container
─  Querying package dependencies
─  Installing package dependencies
─  Running R CMD check
   About to run xvfb-run R CMD check ndl_0.2.18.tar.gz
─  using log directory ‘/home/docker/ndl.Rcheck’
─  using R Under development (unstable) (2018-09-09 r75260)
─  using platform: x86_64-pc-linux-gnu (64-bit)
─  using session charset: UTF-8
✔  checking for file ‘ndl/DESCRIPTION’
─  checking extension type ... Package
─  this is package ‘ndl’ version ‘0.2.18’
✔  checking package namespace information
✔  checking package dependencies
✔  checking if this is a source package
✔  checking if there is a namespace
✔  checking for executable files
✔  checking for hidden files and directories
✔  checking for portable file names
✔  checking for sufficient/correct file permissions
✔  checking whether package ‘ndl’ can be installed
✔  checking installed package size
✔  checking package directory
✔  checking DESCRIPTION meta-information
✔  checking top-level files
✔  checking for left-over files
✔  checking index information
✔  checking package subdirectories
✔  checking R files for non-ASCII characters
✔  checking R files for syntax errors
✔  checking whether the package can be loaded
✔  checking whether the package can be loaded with stated dependencies
✔  checking whether the package can be unloaded cleanly
✔  checking whether the namespace can be loaded with stated dependencies
✔  checking whether the namespace can be unloaded cleanly
✔  checking loading without being on the library search path
✔  checking dependencies in R code
✔  checking S3 generic/method consistency
✔  checking replacement functions
✔  checking foreign function calls
✔  checking R code for possible problems
✔  checking Rd files
✔  checking Rd metadata
✔  checking Rd cross-references
✔  checking for missing documentation entries
✔  checking for code/documentation mismatches
✔  checking Rd \usage sections
✔  checking Rd contents
✔  checking for unstated dependencies in examples
✔  checking contents of ‘data’ directory
N  checking data for non-ASCII characters
     Note: found 7332 marked UTF-8 strings
✔  checking data for ASCII and uncompressed saves
✔  checking line endings in C/C++/Fortran sources/headers
✔  checking line endings in Makefiles
✔  checking compilation flags in Makevars
✔  checking for GNU extensions in Makefiles
✔  checking for portable use of $(BLAS_LIBS) and $(LAPACK_LIBS)
✔  checking compiled code
✔  checking examples
✔  checking PDF version of manual


### rhub (windows-x86_64-devel)


─  Building package
─  Uploading package
─  Preparing build, see status at
   http://builder.r-hub.io/status/ndl_0.2.18.tar.gz-9001be57b54b4af0b63aee0cd1a48ecf
─  Build started
─  Creating new user
─  Downloading and unpacking package file
─  Querying package dependencies
─  Installing package dependencies
─  Running R CMD check
   setting R_REMOTES_NO_ERRORS_FROM_WARNINGS to true
─  using log directory 'C:/Users/USERscGDIFrmpg/ndl.Rcheck'
─  using R Under development (unstable) (2018-07-30 r75016)
─  using platform: x86_64-w64-mingw32 (64-bit)
─  using session charset: ISO8859-1
✔  checking for file 'ndl/DESCRIPTION'
─  checking extension type ... Package
─  this is package 'ndl' version '0.2.18'
✔  checking package namespace information
✔  checking package dependencies
✔  checking if this is a source package
✔  checking for executable files
✔  checking if there is a namespace
✔  checking for hidden files and directories
✔  checking for portable file names
✔  checking whether package 'ndl' can be installed
✔  checking installed package size
✔  checking package directory
✔  checking DESCRIPTION meta-information
✔  checking top-level files
✔  checking for left-over files
✔  checking index information
✔  checking package subdirectories
✔  checking R files for non-ASCII characters
✔  checking R files for syntax errors
─  loading checks for arch 'i386'
✔  ** checking whether the package can be loaded
✔  ** checking whether the package can be loaded with stated dependencies
✔  ** checking whether the package can be unloaded cleanly
✔  ** checking whether the namespace can be loaded with stated dependencies
✔  ** checking whether the namespace can be unloaded cleanly
✔  ** checking loading without being on the library search path
─  loading checks for arch 'x64'
✔  ** checking whether the package can be loaded
✔  ** checking whether the package can be loaded with stated dependencies
✔  ** checking whether the package can be unloaded cleanly
✔  ** checking whether the namespace can be loaded with stated dependencies
✔  ** checking whether the namespace can be unloaded cleanly
✔  ** checking loading without being on the library search path
✔  checking dependencies in R code
✔  checking S3 generic/method consistency
✔  checking replacement functions
✔  checking foreign function calls
✔  checking R code for possible problems
✔  checking Rd files
✔  checking Rd metadata
✔  checking Rd cross-references
✔  checking for missing documentation entries
✔  checking for code/documentation mismatches
✔  checking Rd \usage sections
✔  checking Rd contents
✔  checking for unstated dependencies in examples
✔  checking contents of 'data' directory
N  checking data for non-ASCII characters
     Note: found 7332 marked UTF-8 strings
✔  checking data for ASCII and uncompressed saves
✔  checking line endings in C/C++/Fortran sources/headers
✔  checking line endings in Makefiles
✔  checking compilation flags in Makevars
✔  checking for GNU extensions in Makefiles
✔  checking for portable use of $(BLAS_LIBS) and $(LAPACK_LIBS)
✔  checking compiled code
─  checking examples ...
✔  ** running examples for arch 'i386'
✔  ** running examples for arch 'x64'
✔  checking PDF version of manual

   
── 0 errors ✔ | 0 warnings ✔ | 1 note ✖
─  Done with R CMD check
─  Cleaning up files and user


### macos-elcapitan-release
 
